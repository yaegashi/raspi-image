#!/bin/bash
set -xe

cat <<EOF >/etc/apt/sources.list
deb http://archive.raspbian.org/raspbian buster main contrib non-free rpi
deb http://archive.raspberrypi.org/debian buster main ui
EOF

cat <<EOF >/etc/apt/apt.conf.d/no-install-recommends
APT::Install-Recommends false;
EOF

export DEBIAN_FRONTEND=noninteractive

apt-get update -y
apt-get dist-upgrade -y
apt-get install -y systemd kmod udev dbus busybox wpasupplicant ssh vim-tiny sudo ca-certificates iproute2 iputils-ping

chmod 0600 /etc/ssh/ssh_host_*_key

for i in /lib/modules/*; do
	case $i in
		*-v7+) depmod -a ${i##*/} ;;
		*) rm -rf $i ;;
	esac
done

groupadd -r spi
groupadd -r i2c
groupadd -r gpio
useradd -m -s /bin/bash -G adm,disk,input,audio,video,plugdev,netdev,root,tty,spi,i2c,gpio pi

product="${1-raspberrypi}"
echo "pi:$product" | chpasswd

mkdir /home/pi/.ssh
cp /ssh/pi.id_rsa.pub /home/pi/.ssh/authorized_keys
chown pi.pi -R /home/pi/.ssh
rm -rf /ssh
echo "PasswordAuthentication no" >>/etc/ssh/sshd_config

echo "127.0.1.1 $(cat /etc/hostname)" >>/etc/hosts

ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

systemctl enable loadkmap.service
systemctl enable systemd-networkd.service
systemctl enable wpa_supplicant@wlan0.service
systemctl disable apt-daily.timer
systemctl disable apt-daily-upgrade.timer

echo RuntimeWatchdogSec=14 >>/etc/systemd/system.conf

apt-get clean
rm -rf /git /var/lib/apt/lists/*
rm -f /setup.sh /usr/bin/qemu-arm-static
