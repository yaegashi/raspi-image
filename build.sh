#!/bin/bash

set -eu

TOPDIR=$(cd $(dirname $) && pwd)
cd $TOPDIR
. common.sh

mkdir -p $BUILDPREBUILTDIR
wget https://gitlab.com/yaegashi/raspi-debootstrap/-/jobs/artifacts/master/raw/build/artifacts/raspi-rootfs-buster-armhf.tar.gz?job=build -O $BUILDPREBUILTDIR/rootfs.tar.gz
wget https://gitlab.com/yaegashi/raspi-firmware/-/jobs/artifacts/master/raw/build/artifacts/raspi-boot.tar.gz?job=build -O $BUILDPREBUILTDIR/boot.tar.gz
wget https://gitlab.com/yaegashi/raspi-firmware/-/jobs/artifacts/master/raw/build/artifacts/raspi-modules.tar.gz?job=build -O $BUILDPREBUILTDIR/modules.tar.gz
wget https://gitlab.com/yaegashi/raspi-firmware/-/jobs/artifacts/master/raw/build/artifacts/raspi-vc.tar.gz?job=build -O $BUILDPREBUILTDIR/vc.tar.gz

mkdir -p $BUILDROOTFSDIR
$SUDO tar -C $BUILDROOTFSDIR -xzf $BUILDPREBUILTDIR/rootfs.tar.gz
$SUDO tar -C $BUILDROOTFSDIR -xzf $BUILDPREBUILTDIR/modules.tar.gz
$TAR -C rootfs -cf - . | $SUDO tar -C $BUILDROOTFSDIR -xf -
$SUDO cp -R target/ssh $BUILDROOTFSDIR/ssh
$SUDO cp -R target/keymap $BUILDROOTFSDIR/keymap
$SUDO chroot $BUILDROOTFSDIR /setup.sh $PRODUCT
$SUDO mksquashfs $BUILDROOTFSDIR $BUILDROOTFSFILE -no-fragments

mkdir -p $BUILDBOOTRAMFSDIR/bin
cp target/bin/busybox /usr/bin/qemu-arm-static $BUILDBOOTRAMFSDIR/bin
$SUDO chroot $BUILDBOOTRAMFSDIR qemu-arm-static /bin/busybox --install -s /bin
rm -f $BUILDBOOTRAMFSDIR/bin/qemu-arm-static
tar -C bootramfs -cf - . | $SUDO tar -C $BUILDBOOTRAMFSDIR -xf - 
(cd $BUILDROOTFSDIR && tar -cf - lib/modules/*/kernel/fs/squashfs/squashfs.ko) | $SUDO tar -C $BUILDBOOTRAMFSDIR -xf -
(cd $BUILDBOOTRAMFSDIR && find . | cpio -o --owner 0.0 -H newc) | gzip -c9 > $BUILDBOOTRAMFSFILE

mkdir -p $BUILDBOOTFSDIR
tar -C $BUILDBOOTFSDIR -xf $BUILDPREBUILTDIR/boot.tar.gz
rm -rf $BUILDBOOTFSDIR/bcm2711*
rm -rf $BUILDBOOTFSDIR/start_*
rm -rf $BUILDBOOTFSDIR/start4*
rm -rf $BUILDBOOTFSDIR/fixup_*
rm -rf $BUILDBOOTFSDIR/fixup4*
rm -rf $BUILDBOOTFSDIR/kernel.img
rm -rf $BUILDBOOTFSDIR/kernel7l.img
rm -rf $BUILDBOOTFSDIR/kernel8.img
$TAR -C bootfs -cf - . | tar -C $BUILDBOOTFSDIR -xf -
cp $BUILDBOOTRAMFSFILE $BUILDBOOTFSDIR/initramfs.gz
cp $BUILDROOTFSFILE $BUILDBOOTFSDIR/rootfs.squashfs
cd $BUILDBOOTFSDIR && find . -type f | xargs sha256sum | gzip -c9 >$BUILDDIR/SHA256SUM.txt.gz
mv $BUILDDIR/SHA256SUM.txt.gz $BUILDBOOTFSDIR
$TAR -C $BUILDBOOTFSDIR -czf - . > $BUILDBOOTFSFILE
