: ${PRODUCT=raspi}
: ${VERSION=}
: ${SUDO=sudo}

BUILDDIR=$TOPDIR/build
BUILDARTIFACTSDIR=$BUILDDIR/artifacts
BUILDPREBUILTDIR=$BUILDDIR/prebuilt
BUILDROOTFSDIR=$BUILDDIR/rootfs
BUILDROOTFSFILE=$BUILDROOTFSDIR.squashfs
BUILDBOOTRAMFSDIR=$BUILDDIR/bootramfs
BUILDBOOTRAMFSFILE=$BUILDBOOTRAMFSDIR.gz
BUILDBOOTFSDIR=$BUILDDIR/bootfs
BUILDBOOTFSFILE=$BUILDBOOTFSDIR.tar.gz

TAR="tar --owner 0 --group 0"